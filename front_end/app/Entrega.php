<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entrega extends Model
{
  use SoftDeletes;
  protected $table    = 'entregas';
  protected $fillable = [ 'id', 'observacion', 'policia_encargado', 'placa_auto', 'id_persona' ];
  protected $dates    = ['deleted_at'];
}
