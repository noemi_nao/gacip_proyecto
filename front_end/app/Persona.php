<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Persona extends Model
{
  use SoftDeletes;
  protected $table    = 'personas';
  protected $fillable = ['id', 'nombre', 'domicilio', 'ci', 'celular', 'cantidad_familia', 'distrito', 'zona', 'observacion', 'latitud', 'longitud', 'entregado','ip', 'navegador'];
  protected $dates    = ['deleted_at'];
}
