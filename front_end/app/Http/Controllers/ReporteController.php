<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;
use App\Entrega;

class ReporteController extends Controller
{
    public function getPlanilla(){
      return view('reporte.index');
    }

    public function postPlanilla(Request $request){

      $url = "";
      $datos = "";
      $fechaInicio  = date('Y-m-d', strtotime('-1 day', strtotime($request->fecha_inicio)));
      $fechaFin     = date('Y-m-d', strtotime('+1 day', strtotime($request->fecha_final)));

      $datos = \DB::table('personas')->join('entregas', 'personas.id','=', 'entregas.id_persona')
                                     ->select('personas.*', 'entregas.*')
                                     ->where('personas.created_at', '>', $fechaInicio)
                                     ->where('personas.created_at', '<', $fechaFin)
                                     ->orderby('personas.created_at', 'asc')->get();
      if($request->boton == "Planilla"){
        $url = "reporte.planilla";
      }else if($request->boton == "Reporte"){
        $url = "reporte.reporte";
      }
      return view($url, compact('datos', 'fechaInicio', 'fechaFin'));
    }

}
