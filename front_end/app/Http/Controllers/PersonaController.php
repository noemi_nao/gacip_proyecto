<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;

class PersonaController extends Controller
{
  public function __construct(){
    // $this->middleware('auth');
  }

  public function index(Request $request){
    $datos = Persona::all();
    if ($request->ajax()) {
      return $datos;
    }else{
      return view('persona.index', compact('datos'));
    }
  }

  public function store(Request $request){
    $request['ip']        = \Request::ip();
    if(!isset($request->navegador))
      $request['navegador'] = $request->header('User-Agent');
    if(!isset($request->cantidad_familia))
      $request['cantidad_familia'] = 0;
    if(!isset($request->observacion))
      $request['observacion'] = '.';
    if(!isset($request->entregado))
      $request['entregado'] = "NO";

    $request['user_id']   = 1;//\Auth::user()->id;

    $dato = new Persona;
    $dato->fill($request->all());
    $dato->save();

    return redirect('/Persona');
  }


  public function guardar(Request $request){
    $request   = $request->json()->all();
    $respuesta = array();
    try {
      $dato = new Persona;
      $dato->nombre     = $request['nombre'];
      $dato->domicilio  = $request['domicilio'];
      $dato->ci         = $request['ci'];
      $dato->celular    = $request['celular'];
      $dato->latitud    = $request['latitud'];
      $dato->longitud   = $request['longitud'];
      $dato->entregado  = "proceso"; //proceso, atendido , cancelado
      $dato->ip         = \Request::ip();
      $dato->navegador  = "movil";
      $dato->save();
      $respuesta = array('respuesta'=>'creado', 'id'=>$dato->id);
    } catch (\Exception $e) {
      $respuesta = array('respuesta'=>'erro', 'id'=>'0');
    }
    return $respuesta;
  }

  public function getEstado($id){
    $dato = Persona::find($id);
    $respuesta = ["respuesta"=>$dato->entregado];
    return $respuesta;

  }

  public function show($id){
    $datos = Persona::Where('id', '=', $id)->get();
    return $datos;
  }

  public function update(Request $request, $id){
    $dato = Persona::find($id);
    $request['user_id'] = 1;//\Auth::user()->id;
    $dato->fill($request->all());
    $dato->save();
    return redirect('/Persona');
  }

  public function destroy(Request $request, $id){
    if( $request->ajax() ){
      $dato = Persona::find($id);
      $dato->delete();
      return "Persona Eliminada";
    }else{
      return redirect('/');
    }
  }

  public function entregaGet(){
    $datos = Persona::all();
    return view('persona.movil', compact('datos'));
  }

  public function entregaPost(Request $request, $id){
    //return $request->all();
    $dato = Persona::find($id);
    $dato->distrito         = $request->distrito;
    $dato->cantidad_familia = $request->cantidad_familia;
    $dato->zona             = $request->zona;
    $dato->observacion      = $request->observacion;
    $dato->entregado        = "atendido";
    $dato->save();

    $request['user_id'] = 1;//\Auth::user()->id;
    $dato = new \App\Entrega;
    $dato->observacion        = $request->observacion;
    $dato->policia_encargado  = $request->policia_encargado;
    $dato->placa_auto         = $request->placa_auto;
    $dato->id_persona         = $id;
    $dato->save();
    return redirect('/Entrega/Celular');
  }

  public function getCancelar($id){
    $dato = Persona::find($id);
    $dato->entregado="cancelar";
    $dato->save();
    return redirect('/Persona');
  }

}
