@extends('movil')

@section('persona') style='background-color:white; color:balck; '  @endsection
@section('titulo') Tesoreria, Tipos de Boletas @endsection


@section('titulo') Menu @endsection
@section('direccion') <a href="{{asset('/index.php/Ingrediente')}}"> <span class="nav-link-text">Personas</span> </a> @endsection



@section('modal2')
    <div id="modalModifiar" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content panel panel-warning">
                <div class="modal-header panel-heading">
                    <b>Actualizar</b>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body panel-body">
                    {!! Form::open(['route'=>['entrega.post', ':DATO_ID'], 'method'=>'PATCH', 'id'=>'form-update' ])!!}

                    <div class="form-group">
                      <label for="nombre" >Nombre</label>
                      {!! Form::text('nombre',  null, ['class'=>'form-control', 'placeholder'=>'Nombre', 'id'=>'nombre', 'readonly']) !!}
                    </div>
                    <div class="form-group">
                      <label for="domicilio" >Domicilio</label>
                      {!! Form::text('domicilio', null, ['class'=>'form-control', 'placeholder'=>'Domicilio', 'id'=>'domicilio', 'readonly']) !!}
                    </div>
                    <div class="form-group">
                      <label for="celular" >celular</label>
                      {!! Form::text('celular', null, ['class'=>'form-control', 'placeholder'=>'celular', 'id'=>'celular', 'readonly']) !!}
                    </div>

                    <div class="form-group">
                      <label for="ci" >CI</label>
                      {!! Form::number('ci', null, ['class'=>'form-control', 'placeholder'=>'CI', 'id'=>'ci', 'required', 'readonly']) !!}
                    </div>

                    <div class="form-group">
                      <label for="distrito" >Distrito</label>
                      {!! Form::number('distrito', null, ['class'=>'form-control', 'placeholder'=>'Distrito', 'id'=>'distrito', 'required']) !!}
                    </div>

                    <div class="form-group">
                      <label for="cantidad_familia" >C. Familia</label>
                      {!! Form::number('cantidad_familia', null, ['class'=>'form-control', 'placeholder'=>'C. Familia', 'id'=>'cantidad_familia', 'required']) !!}
                    </div>


                    <div class="form-group">
                      <label for="policia_encargado" >Encargado</label>
                      {!! Form::text('policia_encargado', null, ['class'=>'form-control', 'placeholder'=>'Encargado', 'id'=>'policia_encargado', 'required']) !!}
                    </div>

                    <div class="form-group">
                      <label for="placa_auto" >Placa</label>
                      {!! Form::text('placa_auto', null, ['class'=>'form-control', 'placeholder'=>'Placa', 'id'=>'placa_auto', 'required']) !!}
                    </div>


                    <div class="form-group">
                      <label for="zona" >Zona</label>
                      {!! Form::text('zona', null, ['class'=>'form-control', 'placeholder'=>'Zona', 'id'=>'zona', 'list'=>'lista-zona', 'required']) !!}
                      <datalist id="lista-zona">
                        <option value="ZONA ALTA">
                        <option value="ZONA BAJA">
                        <option value="LADERA IZQUIERDA">
                        <option value="LADERA DERECHA">
                      </datalist>
                    </div>

                    <div class="form-group">
                      <label for="observacion">Observacion</label>
                      {!! Form::textarea('observacion', null, ['class'=>'form-control', 'placeholder'=>'Observacion', 'id'=>'observacion']) !!}
                    </div>



                    {!! Form::submit('Entregar ', ['class'=>'btn btn-warning']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection



@section('cuerpo')
        <table id="tablaAgenda" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>&nbsp;</th>
              <th>Nombre</th>
              <th>Domicilio</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            @foreach($datos as $dato)

            @if($dato->entregado[0] == "p" )
              <tr data-id="{{ $dato->id }}" style="background-color:#8FFD95;">
            @elseif($dato->entregado[0] == "c" )
              <tr data-id="{{ $dato->id }}" style="background-color:#F74242;">
            @elseif($dato->entregado[0] == "a" )
              <tr data-id="{{ $dato->id }}">
            @endif
              <td>
                <a href="#modalModifiar"  data-toggle="modal" data-target="" class="actualizar" style="color: #B8823B;"> <li class="fa fa-edit"></li></a>
              </td>
              <td> {{ $dato->entregado[0] }} {{$dato->nombre}}   </td>
              <td>{{$dato->domicilio}}</td>
              <td>
                <a target="_blank" href="https://www.google.com/maps?q={{$dato->latitud}},{{$dato->longitud}}" style="color:#0d6d08;" class="eliminar"> <li class="fa fa-map"></li></a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>

@endsection

@section('js')
<script type="text/javascript">

  $(document).ready(function(){
    $('#tablaAgenda').DataTable({
      "order": [[ 1, 'desc']],
      "language": {
        "bDeferRender": true,
        "sEmtpyTable": "No ay registros",
        "decimal": ",",
        "thousands": ".",
        "lengthMenu": " ",
        "zeroRecords": "No se encontro nada,  lo siento",
        "info": "Mostrar paginas [_PAGE_] de [_PAGES_]",
        "infoEmpty": "No ay entradas permitidas",
        "search": "Buscar ",
        "infoFiltered": "(Busqueda de _MAX_ registros en total)",
        "oPaginate":{
          "sLast":"Final",
          "sFirst":"Principio",
          "sNext":"Siguiente",
          "sPrevious":"Anterior"
        }
      }
    });
  });

  $('.actualizar').click(function(event){
    event.preventDefault();
    var fila = $(this).parents('tr');
    var id = fila.data('id');
    var form = $('#form-update')
    var url = form.attr('action').replace(':DATO_ID', id);
    form.get(0).setAttribute('action', url);
    link  = '{{ asset("/index.php/Persona/")}}/'+id;

    $.getJSON(link, function(data, textStatus) {
      if(data.length > 0){
        $.each(data, function(index, el) {
          $('#nombre').val(el.nombre);
          $('#domicilio').val(el.domicilio);
          $('#ci').val(el.ci);

          $('#celular').val(el.celular);
          $('#cantidad_familia').val(el.cantidad_familia);
          $('#distrito').val(el.distrito);
          $('#zona').val(el.zona);
          $('#observacion').val(el.observacion);
          $('#entregado').val(el.entregado);

        });
      }
    });
  });



</script>
@endsection
