@extends('gacip')

@section('persona') style='background-color:white; color:balck; '  @endsection
@section('titulo') Tesoreria, Tipos de Boletas @endsection


@section('titulo') Menu @endsection
@section('direccion') <a href="{{asset('/index.php/Ingrediente')}}"> <span class="nav-link-text">Personas</span> </a> @endsection

@section('modal1')
<div id="modalAgregar" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content panel panel-primary">

      <div class="modal-header panel-heading">
        <b>Insertar</b>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div class="modal-body panel-body">
        {!! Form::open(['accept-charset'=>'UTF-8', 'enctype'=>'multipart/form-data', 'method'=>'POST', 'files'=>true, 'autocomplete'=>'off', 'id'=>'form-insert'] ) !!}
        <div class="form-group">
          <label for="nombre_" >Nombre</label>
          {!! Form::text('nombre',  null, ['class'=>'form-control', 'placeholder'=>'Nombre', 'id'=>'nombre_', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="domicilio_" >Domicilio</label>
          {!! Form::text('domicilio', null, ['class'=>'form-control', 'placeholder'=>'Domicilio', 'id'=>'domicilio_', 'required']) !!}
        </div>
        <div class="form-group">
          <label for="celular" >celular</label>
          {!! Form::text('celular', null, ['class'=>'form-control', 'placeholder'=>'celular', 'id'=>'cantidad_personas_']) !!}
        </div>

        <div class="form-group">
          <label for="ci_" >CI</label>
          {!! Form::number('ci', null, ['class'=>'form-control', 'placeholder'=>'CI', 'id'=>'ci_', 'required']) !!}
        </div>

        <div class="form-group">
          <label for="distrito_" >Distrito</label>
          {!! Form::number('distrito', null, ['class'=>'form-control', 'placeholder'=>'Distrito', 'id'=>'distrito_', 'required']) !!}
        </div>


        <div class="form-group">
          <label for="zona_" >Zona</label>
          {!! Form::text('zona', null, ['class'=>'form-control', 'placeholder'=>'Zona', 'id'=>'zona_', 'list'=>'lista-zona']) !!}
          <datalist id="lista-zona">
            <option value="ZONA ALTA">
            <option value="ZONA BAJA">
            <option value="LADERA IZQUIERDA">
            <option value="LADERA DERECHA">
          </datalist>
        </div>

        {!! Form::hidden('cantidad_familia', '0') !!}
        {!! Form::hidden('observacion', '.') !!}
        {!! Form::hidden('latitud', '0') !!}
        {!! Form::hidden('longitud', '0') !!}
        {!! Form::hidden('entregado', 'NO') !!}

        {!! Form::submit('A&ntilde;adir', ['class'=>'agregar btn btn-primary']) !!}
        {!! Form::close() !!}
      </div>

    </div>
  </div>
</div>
@endsection

@section('modal2')
    <div id="modalModifiar" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content panel panel-warning">
                <div class="modal-header panel-heading">
                    <b>Actualizar</b>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body panel-body">
                    {!! Form::open(['route'=>['Persona.update', ':DATO_ID'], 'method'=>'PATCH', 'id'=>'form-update' ])!!}

                    <div class="form-group">
                      <label for="nombre" >Nombre</label>
                      {!! Form::text('nombre',  null, ['class'=>'form-control', 'placeholder'=>'Nombre', 'id'=>'nombre', 'readonly']) !!}
                    </div>
                    <div class="form-group">
                      <label for="domicilio" >Domicilio</label>
                      {!! Form::text('domicilio', null, ['class'=>'form-control', 'placeholder'=>'Domicilio', 'id'=>'domicilio', 'readonly']) !!}
                    </div>
                    <div class="form-group">
                      <label for="celular" >celular</label>
                      {!! Form::text('celular', null, ['class'=>'form-control', 'placeholder'=>'celular', 'id'=>'celular', 'readonly']) !!}
                    </div>

                    <div class="form-group">
                      <label for="ci" >CI</label>
                      {!! Form::number('ci', null, ['class'=>'form-control', 'placeholder'=>'CI', 'id'=>'ci', 'readonly']) !!}
                    </div>

                    <div class="form-group">
                      <label for="distrito" >Distrito</label>
                      {!! Form::number('distrito', null, ['class'=>'form-control', 'placeholder'=>'Distrito', 'id'=>'distrito', 'required']) !!}
                    </div>

                    <div class="form-group">
                      <label for="cantidad_familia" >C. Familia</label>
                      {!! Form::number('cantidad_familia', null, ['class'=>'form-control', 'placeholder'=>'C. Familia', 'id'=>'cantidad_familia', 'required']) !!}
                    </div>

                    <div class="form-group">
                      <label for="entregado">Entregado</label>
                      {!! Form::text('entregado', null, ['class'=>'form-control', 'placeholder'=>'', 'id'=>'entregado', 'required']) !!}
                    </div>

                    <div class="form-group">
                      <label for="zona" >Zona</label>
                      {!! Form::text('zona', null, ['class'=>'form-control', 'placeholder'=>'Zona', 'id'=>'zona', 'list'=>'lista-zona', 'required']) !!}
                      <datalist id="lista-zona">
                        <option value="ZONA ALTA">
                        <option value="ZONA BAJA">
                        <option value="LADERA IZQUIERDA">
                        <option value="LADERA DERECHA">
                      </datalist>
                    </div>

                    <div class="form-group">
                      <label for="observacion">Observacion</label>
                      {!! Form::textarea('observacion', null, ['class'=>'form-control', 'placeholder'=>'Observacion', 'id'=>'observacion', 'required']) !!}
                    </div>

                    {!! Form::submit('Actualizar ', ['class'=>'btn btn-warning']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection


@section('cuerpo')
<div class="row" class="scrollmenu">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-body">
        <table id="tablaAgenda" class="table display" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th>Nombre</th>
              <th>Domicilio</th>
              <th>Celular</th>
              <th>Zona</th>
              <th>Entregado</th>
              <th>Acciones</th>
            </tr>
          </thead>
          <tbody>
            @foreach($datos as $dato)
            @if($dato->entregado[0] == "p" )
              <tr data-id="{{ $dato->id }}" style="background-color:#8FFD95;">
            @elseif($dato->entregado[0] == "c" )
              <tr data-id="{{ $dato->id }}" style="background-color:#f69898 ;">
            @elseif($dato->entregado[0] == "a" )
              <tr data-id="{{ $dato->id }}">
            @endif
              <td>{{$dato->nombre}}   </td>
              <td>{{$dato->domicilio}}</td>
              <td>{{$dato->celular}}  </td>
              <td>{{$dato->zona}}     </td>
              <td>{{$dato->entregado}}</td>
              <td>
                <a href="#modalModifiar"  data-toggle="modal" data-target="" class="actualizar" style="color: #B8823B;"> <li class="fa fa-edit"></li></a>
                <a target="_blank" href="https://www.google.com/maps?q={{$dato->latitud}},{{$dato->longitud}}" style="color:#0d6d08;" > <li class="fa fa-map"></li></a>
                <a href="#"  data-toggle="modal" data-target="" style="color: #ff0000;" onclick="elimnar({{$dato->id}})"> <li class="fa fa-trash"></li></a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {!! Form::open(['route'=>['Persona.destroy', ':DATO_ID'], 'method'=>'DELETE', 'id'=>'form-delete']) !!}
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')
<script type="text/javascript">

  $(document).ready(function(){
    $('#tablaAgenda').DataTable({
      "order": [[ 4, 'desc']],
      "language": {
        "bDeferRender": true,
        "sEmtpyTable": "No ay registros",
        "decimal": ",",
        "thousands": ".",
        "lengthMenu": "Mostrar _MENU_ datos por registros",
        "zeroRecords": "No se encontro nada,  lo siento",
        "info": "Mostrar paginas [_PAGE_] de [_PAGES_]",
        "infoEmpty": "No ay entradas permitidas",
        "search": "Buscar ",
        "infoFiltered": "(Busqueda de _MAX_ registros en total)",
        "oPaginate":{
          "sLast":"Final",
          "sFirst":"Principio",
          "sNext":"Siguiente",
          "sPrevious":"Anterior"
        }
      }
    });
  });

  $('.actualizar').click(function(event){
    event.preventDefault();
    var fila = $(this).parents('tr');
    var id = fila.data('id');
    var form = $('#form-update')
    var url = form.attr('action').replace(':DATO_ID', id);
    form.get(0).setAttribute('action', url);
    link  = '{{ asset("/index.php/Persona/")}}/'+id;

    $.getJSON(link, function(data, textStatus) {
      if(data.length > 0){
        $.each(data, function(index, el) {
          $('#nombre').val(el.nombre);
          $('#domicilio').val(el.domicilio);
          $('#ci').val(el.ci);

          $('#celular').val(el.celular);
          $('#cantidad_familia').val(el.cantidad_familia);
          $('#distrito').val(el.distrito);
          $('#zona').val(el.zona);
          $('#observacion').val(el.observacion);
          $('#entregado').val(el.entregado);

        });
      }
    });
  });

  function elimnar(id){
    var link = "{{asset('index.php/Persona/Cancelar/')}}/"+id;
    if(confirm('Esta seguro de CANCELAR el REGISTRO')){
      location.href=link;
    }
  }

  $('.eliminar').click(function(event) {
    event.preventDefault();
    var fila = $(this).parents('tr');
    var id = fila.data('id');
    var form = $('#form-delete');
    var url = form.attr('action').replace(':DATO_ID',id);
    var data = form.serialize();
    console.log(data);

    if(confirm('Esta seguro de CANCELAR el REGISTRO')){
      $.post(url, data, function(result, textStatus, xhr) {
        alert(result);
        fila.fadeOut();
      }).fail(function(esp){
        fila.show();
      });
    }
  });

</script>
@endsection
