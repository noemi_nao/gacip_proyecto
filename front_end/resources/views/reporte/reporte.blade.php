<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
    table {
        border-collapse: collapse;
        }

        table, th, td {
        border: 1px solid black;
        }
    </style>
  </head>
  <body>
    <table width="100%" border="1">
      <thead>
        <tr>
          <th colspan="8"> <h2>G.A.C.I.P. Potosí</h2><br>REPORTE</th>
        </tr>
        <tr>
          <td colspan="4"><small>Planilla de fecha: {{$fechaInicio}} {{$fechaFin}}</small></td>
          <td colspan="4"><small>Planilla Generada en: {{ date('d-m-Y h:i:s') }}</small></td>
        </tr>
        <tr>
          <th>N°</th>
          <th>Nombres y Apellidos</th>
          <th>Distrito</th>
          <th>Domicilio</th>
          <th>Estado</th>
          <th>Encargado</th>
          <th>Placa</th>
          <th>Observacio</th>
        </tr>
      </thead>
      <tbody><?php $i=1;?>
        @foreach($datos as $dato)
        <tr >
          <td>{{$i}}</td>
          <td>{{$dato->nombre}}</td>
          <td>{{$dato->distrito}}</td>
          <td>{{$dato->domicilio}}</td>
          <td>{{$dato->entregado}}</td>
          <td>{{$dato->policia_encargado}}</td>
          <td>{{$dato->placa_auto}}</td>
          <td>{{$dato->observacion}}</td>
        </tr><?php $i++;?>
        @endforeach
      </tbody>
    </table>
  </body>
</html>
