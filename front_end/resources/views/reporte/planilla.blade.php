<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <style media="screen">
    table {
        border-collapse: collapse;
        }

        table, th, td {
        border: 1px solid black;
        }
    </style>
  </head>
  <body>
    <table width="100%" border="1">
      <thead>
        <tr>
          <th colspan="6"> <h2>G.A.C.I.P. Potosí</h2></th>
        </tr>
        <tr>
          <td colspan="3"><small>Planilla de fecha: {{$fechaInicio}} {{$fechaFin}}</small></td>
          <td colspan="3"><small>Planilla Generada en: {{ date('d-m-Y h:i:s') }}</small></td>
        </tr>
        <tr>
          <th>N°</th>
          <th>NOMBRES Y APELLIDOS</th>
          <th>C.I.</th>
          <th>Domicilio</th>
          <th>Telefono</th>
          <th>Firma</th>
        </tr>
      </thead>
      <tbody><?php $i=1;?>
        @foreach($datos as $dato)
        <tr height="50px;">
          <td>{{$i}}</td>
          <td>{{$dato->nombre}}</td>
          <td>{{$dato->ci}}</td>
          <td>{{$dato->domicilio}}</td>
          <td>{{$dato->celular}}</td>
          <td>....</td>
        </tr><?php $i++;?>
        @endforeach
      </tbody>
    </table>
  </body>
</html>
