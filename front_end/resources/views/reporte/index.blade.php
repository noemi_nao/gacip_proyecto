@extends('gacip')

@section('reporte') style='background-color:white; color:balck; '  @endsection
@section('titulo') Tesoreria, Tipos de Boletas @endsection


@section('titulo') Menu @endsection
@section('direccion') <a href="{{asset('/index.php/Reporte')}}"> <span class="nav-link-text">Reporte</span> </a> @endsection

@section('cuerpo')

{!! Form::open(['accept-charset'=>'UTF-8', 'enctype'=>'multipart/form-data', 'method'=>'POST', 'files'=>true, 'autocomplete'=>'off', 'id'=>'form-insert'] ) !!}
<div class="row">
  <div class="col-md-6">
    <label for=""> <b>Fecha Inicio</b> </label>
    {!! Form::date('fecha_inicio', null, ['class'=>'form-control', 'required']) !!}
  </div>
  <div class="col-md-6">
    <label for=""> <b>Fecha Final</b> </label>
    {!! Form::date('fecha_final', null, ['class'=>'form-control', 'required']) !!}
  </div>
</div>

<div class="row">
  <div class="col-md-4">
    <input type="submit" name="boton" value="Planilla"  class="form-control btn btn-danger">
  </div>
  <div class="col-md-4">
    &nbsp;
  </div>
  <div class="col-md-4">
    <input type="submit" name="boton" value="Reporte"  class="form-control btn btn-primary">
  </div>
</div>
{!! Form::close() !!}

@endsection
