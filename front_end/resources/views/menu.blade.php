<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">

  <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables" @yield('persona')>
    <a class="nav-link" href="{{asset('/index.php/Persona')}}">
      <i class="fa fa-fw fa-user"></i>
      <span class="nav-link-text">Persona</span>
    </a>
  </li>

  <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables" @yield('reporte')>
    <a class="nav-link" href="{{asset('/index.php/Reporte')}}">
      <i class="fa fa-fw fa-table"></i>
      <span class="nav-link-text">Reporte</span>
    </a>
  </li>

  <!--<li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables" @yield('tiempo')>
    <a class="nav-link" href="{{asset('/index.php/Tiempo')}}">
      <i class="fa fa-fw fa-clock"></i>
      <span class="nav-link-text">Estadistica</span>
    </a>
  </li>-->

</ul>
