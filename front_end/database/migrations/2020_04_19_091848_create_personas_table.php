<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('domicilio')->nullable();
            $table->string('ci', 25)->nullable();
            $table->string('celular', 25)->nullable();

            $table->integer('cantidad_familia')->nullable();
            $table->integer('distrito')->nullable();
            $table->string('zona')->nullable();
            $table->string('observacion')->nullable();

            $table->string('latitud')->nullable();
            $table->string('longitud')->nullable();
            $table->string('entregado',10)->comment('SI/NO');

            $table->string('ip');
            $table->string('navegador');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
