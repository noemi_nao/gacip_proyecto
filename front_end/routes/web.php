<?php

Route::get('/', function () {
    return view('gacip');
});
Route::get('/home',function () {
    return view('gacip');
});

/* Inisio de Session
//Route::Auth();*/
/*
Route::get('login', 'Auth\AuthController@showLoginForm');
Route::post('login', 'Auth\AuthController@login');
Route::get('logout', 'Auth\AuthController@logout');
*/
/* Administracion de Usuarios */
Route::get('usuarios', 'UsuarioController@index');
Route::get('usuarios/create', 'UsuarioController@showRegistrationForm');
Route::post('usuarios', 'UsuarioController@create');
Route::get('usuarios/{id}', 'UsuarioController@viewuser');
Route::get('usuarios/{id}/edit', 'UsuarioController@edit');
Route::patch('usuarios/{id}', 'UsuarioController@update');
Route::get('usuarios/info/ver', 'UsuarioController@profile');
Route::post('usuarios/info/ver', 'UsuarioController@profileActulizar');

Route::resource('Persona',   'PersonaController');
Route::get('Persona/Cancelar/{id}',   'PersonaController@getCancelar');

Route::post('Movil/Guardar',   'PersonaController@guardar')->name('guarda.movil');
Route::get('Movil/Estado/{id}',   'PersonaController@getEstado')->name('estado.movil');

Route::get('Entrega/Celular',   'PersonaController@entregaGet')->name('entrega.get');
Route::patch('Entrega/Celular/{Persona}',   'PersonaController@entregaPost')->name('entrega.post');
//Route::resource('Entrega', 'EntregaController');

Route::get('Reporte', 'ReporteController@getPlanilla')->name('reporte.ver');
Route::post('Reporte', 'ReporteController@postPlanilla')->name('reporte.generar');
