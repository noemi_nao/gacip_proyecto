package com.gacip.models;

public class Solicitud {

    private String nombre;
    private String ci;
    private String direccion;
    private String telefono;
    private double latitud;
    private double longitude;
    public  Solicitud(){

        this.latitud = 0.0;
        this.longitude = 0.0;
    }

    public Solicitud(String nombre, String ci, String direccion, String telefono, double latitud, double longitude) {
        this.nombre = nombre;
        this.ci = ci;
        this.direccion = direccion;
        this.telefono = telefono;
        this.latitud = latitud;
        this.longitude = longitude;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
