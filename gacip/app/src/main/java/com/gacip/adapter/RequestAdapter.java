package com.gacip.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.gacip.R;
import com.gacip.gacip.db.Solicitud;

import java.util.ArrayList;

public class RequestAdapter extends ArrayAdapter<com.gacip.gacip.db.Solicitud> implements  View.OnClickListener {

    private ArrayList<com.gacip.gacip.db.Solicitud> dataSet;
    Context mContext;
    private static class ViewHolder {
        TextView txtName;
        TextView txtAddress;
        TextView txtCi;
        TextView txtCelular;
        TextView txtState;

    }

    public RequestAdapter(ArrayList<com.gacip.gacip.db.Solicitud> data, Context context) {
        super(context, R.layout.request_item, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public void onClick(View v) {

        int position=(Integer) v.getTag();
        Object object= getItem(position);
        //com.gacip.gacip.db.Solicitud dataModel=(Solicitud)object;


    }

    private int lastPosition = -1;

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Solicitud dataModel =  dataSet.get(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.request_item, parent, false);

            viewHolder.txtName = convertView.findViewById(R.id.txt_name);
            viewHolder.txtAddress = convertView.findViewById(R.id.txt_address);
            viewHolder.txtCi = convertView.findViewById(R.id.txt_ci);
            viewHolder.txtCelular= convertView.findViewById(R.id.txt_celular);
            viewHolder.txtState = convertView.findViewById(R.id.txt_state);

            result = convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        lastPosition = position;

        viewHolder.txtName.setText(dataModel.getNombre());
        viewHolder.txtAddress.setText(dataModel.getDireccion());
        viewHolder.txtCelular.setText(dataModel.getTelefono());
        viewHolder.txtCi.setText(dataModel.getCi());
       // viewHolder.txtState.setText("En proceso");
        return convertView;
    }
}
