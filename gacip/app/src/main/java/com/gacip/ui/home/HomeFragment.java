package com.gacip.ui.home;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.gacip.MainApplication;
import com.gacip.R;
import com.gacip.gacip.db.DaoSession;
import com.gacip.gacip.db.SolicitudDao;
import com.gacip.models.Solicitud;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private Solicitud miSolicitud;
    private static  String URL_SERVER = "https://gacip.potosi.bo/index.php/Movil/Guardar";
    int PERMISSION_ID = 44;
    FusedLocationProviderClient mFusedLocationClient;
    private TextView txtName;
    private EditText txtAddress;
    private EditText txtCi;
    private EditText txtCelular;
    private TextView txt_ubicacion;
    private double mlatitude = 0.0;
    private  double mlongitude= 0.0 ;

    private SolicitudDao solicitudDao;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        getLastLocation();
        DaoSession daoSession = ((MainApplication)getActivity().getApplication()).getDaoSession();
        solicitudDao = daoSession.getSolicitudDao();
    }

    @SuppressLint("MissingPermission")
    private void getLastLocation(){
        if (checkPermissions()) {
            if (isLocationEnabled()) {
                mFusedLocationClient.getLastLocation().addOnCompleteListener(
                        new OnCompleteListener<Location>() {
                            @Override
                            public void onComplete(@NonNull Task<Location> task) {
                                Location location = task.getResult();
                                if (location == null) {
                                    requestNewLocationData();
                                } else {
                                    mlatitude = location.getLatitude();
                                    mlongitude = location.getLongitude();
                                    txt_ubicacion.setText("Tu ubicación es: "+location.getLatitude()+ ":"+ location.getLongitude());
                                }
                            }
                        }
                );
            } else {
                Toast.makeText(getActivity(), "Activa tu GPS", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        } else {
            requestPermissions();
        }
    }



    @SuppressLint("MissingPermission")
    private void requestNewLocationData(){

        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(0);
        mLocationRequest.setFastestInterval(0);
        mLocationRequest.setNumUpdates(1);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        mFusedLocationClient.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
        );

    }

    private LocationCallback mLocationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            Location mLastLocation = locationResult.getLastLocation();
            mlatitude = mLastLocation.getLatitude();
            mlongitude = mLastLocation.getLongitude();
            txt_ubicacion.setText("Tu ubicación es: "+mLastLocation.getLatitude()+ ":"+ mLastLocation.getLongitude());
        }
    };

    private boolean checkPermissions() {
        return ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        ActivityCompat.requestPermissions(
                getActivity(),
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION},
                PERMISSION_ID
        );
    }

    private boolean isLocationEnabled() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
                LocationManager.NETWORK_PROVIDER
        );
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_ID) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation();
            }
        }
    }
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        final TextView textView = root.findViewById(R.id.message);
        final Button btn_register = root.findViewById(R.id.bt_register);
        final Button btn_cancelar = root.findViewById(R.id.btn_cancelar);
        txtName = root.findViewById(R.id.edt_name);
        txtAddress = root.findViewById(R.id.edt_address);
        txtCi = root.findViewById(R.id.edt_ci);
        txtCelular = root.findViewById(R.id.edt_celular);
        txt_ubicacion = root.findViewById(R.id.txt_ubicación);




        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });


        btn_register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                miSolicitud = new Solicitud();
                miSolicitud.setLatitud(mlatitude);
                miSolicitud.setLongitude(mlongitude);
                if (txtName.getText().toString().length() != 0 ){
                    miSolicitud.setNombre(txtName.getText().toString());
                }else{
                    Toast.makeText(getActivity(),"Ingrese un nombre, válido.", Toast.LENGTH_LONG).show();
                }
                if (txtCi.getText().toString().length() != 0 ){
                    miSolicitud.setCi(txtCi.getText().toString());
                }else{
                    Toast.makeText(getActivity(),"Ingrese una cédula de identidad, válida.", Toast.LENGTH_LONG).show();
                }
                if (txtAddress.getText().toString().length() != 0 ){
                    miSolicitud.setDireccion(txtAddress.getText().toString());
                    }else{
                    Toast.makeText(getActivity(),"Ingrese una dirección, válida.", Toast.LENGTH_LONG).show();
                }
                if (txtCelular.getText().toString().length() != 0 ){
                    miSolicitud.setTelefono(txtCelular.getText().toString());
                }else{
                    Toast.makeText(getActivity(),"Ingrese un número de celular, válido.", Toast.LENGTH_LONG).show();
                }

                String jsonObject =     "{\"nombre\":"+"\""+miSolicitud.getNombre()+"\""+
                        ", \"domicilio\":"+"\""+miSolicitud.getDireccion()+"\""+
                        ", \"ci\":"+"\""+miSolicitud.getCi()+"\""+
                        ", \"celular\":"+"\""+miSolicitud.getTelefono()+"\""+", \"latitud\": "+"\""+miSolicitud.getLatitud()+"\""+", \"longitud\":"+miSolicitud.getLongitude()+"}";


                if (miSolicitud.getNombre()!= null  && miSolicitud.getCi()!= null && miSolicitud.getDireccion()!= null ){
                    new sendHelpRequestService().execute(jsonObject);

                }else{
                    showConfirmation("Su solicitud no se envio, ingrese información válida.");
                }
            }
        });

        btn_cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             clearForm();
            }
        });

        return root;
    }

    @Override
    public void onResume(){
        super.onResume();
        if (checkPermissions()) {
            getLastLocation();
        }

    }

    @SuppressLint("StaticFieldLeak")
    class sendHelpRequestService extends AsyncTask<String, Void, String>{
        ProgressDialog progressDialog = null;
        String result = "";
        Boolean state = false;
        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
            if (progressDialog == null) {
                progressDialog = new ProgressDialog(getActivity());
                progressDialog.setMessage("Enviando...");
                progressDialog.setIndeterminate(false);
                progressDialog.setCancelable(false);
                progressDialog.show();
            }
        }
        @Override
        protected String doInBackground(String...  params) {
            // TODO Auto-generated method stub
            result = sendHelp(params[0]);
            try {

                JSONObject obj = new JSONObject(result);
                if (obj.getString("respuesta").equals("creado")){
                    state = true;
                }

            } catch (Throwable t) {
                Log.d("error", "error de json: \"" + result + "\"");
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {
            // TODO Auto-generated method stub
            super.onPostExecute(result);
            if(progressDialog.isShowing() && progressDialog != null){
                progressDialog.dismiss();
                progressDialog = null;
            }
            if (state ){
                showConfirmation("Su  solicitud de ayuda se envio, el G.A.C.I.P se comunicará con usted.");
                clearForm();
                addSolicitud(miSolicitud);
            }else{
                showConfirmation("Su solicitud no se envio, \n intente mas tarde.");
            }


        }

    }

    private String sendHelp(String params){

        HttpClient httpclient=new DefaultHttpClient();
        HttpPost httppost=new HttpPost(URL_SERVER);
        String result = "";
        try {

            StringEntity requestObject = new StringEntity(params);
           
            httppost.setEntity(requestObject);

            HttpResponse httpresponse=httpclient.execute(httppost);
            int status=httpresponse.getStatusLine().getStatusCode();
            if (status == 200) {
                InputStream inputStream = httpresponse.getEntity().getContent();
                BufferedReader reader = new BufferedReader
                        (new InputStreamReader(inputStream));
                String line;
                while ((line = reader.readLine()) != null) {
                    result += line;
                }
            }else{
                showConfirmation("Su solicitud no se envio, intente nuevamente.");
            }
            Log.d("RESULT",result);

        } catch (ClientProtocolException e) {
            Log.d("ERROR","Error al enviar los datos al servidor");
        } catch (IOException e) {
            Log.d("ERROR","Error al enviar los datos al servidor");
        }

        return result;

    }

    private void showConfirmation(String mensage){
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(mensage);
        builder.setCancelable(true);

        builder.setPositiveButton(
                "Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert = builder.create();
        alert.show();

    }

    private void clearForm(){
        txtAddress.setText("");
        txtName.setText("");
        txtCi.setText("");
        txtCelular.setText("");
        txt_ubicacion.setText("");
    }

    private void addSolicitud(Solicitud solicitud){
        com.gacip.gacip.db.Solicitud solitudObj = new com.gacip.gacip.db.Solicitud();
        solitudObj.setNombre(solicitud.getNombre());
        solitudObj.setCi(solicitud.getCi());
        solitudObj.setDireccion(solicitud.getDireccion());
        solitudObj.setTelefono(solicitud.getTelefono());
        solitudObj.setLatitud(String.valueOf(solicitud.getLatitud()));
        solitudObj.setLongitude(String.valueOf(solicitud.getLongitude()));
        solicitudDao.insertOrReplace(solitudObj);
    }

}
