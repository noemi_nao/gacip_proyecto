package com.gacip.ui.request;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.ListFragment;
import androidx.lifecycle.ViewModelProviders;

import com.gacip.MainApplication;
import com.gacip.R;
import com.gacip.adapter.RequestAdapter;
import com.gacip.gacip.db.DaoSession;
import com.gacip.gacip.db.SolicitudDao;
import com.gacip.models.Solicitud;

import java.util.ArrayList;

public class MyRequestFragment extends ListFragment implements AdapterView.OnItemClickListener {

    private MyRequestViewModel requestViewModel;
    private SolicitudDao solicitudDao;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        requestViewModel =
                ViewModelProviders.of(this).get(MyRequestViewModel.class);
        View root = inflater.inflate(R.layout.fragment_list_request, container, false);
        DaoSession daoSession = ((MainApplication)getActivity().getApplication()).getDaoSession();
        solicitudDao = daoSession.getSolicitudDao();
        //final TextView textView = root.findViewById(R.id.text_gallery);
//        galleryViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//                textView.setText(s);
//            }
//        });
        return root;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ArrayList<com.gacip.gacip.db.Solicitud> data = new ArrayList<>();
        data = (ArrayList<com.gacip.gacip.db.Solicitud>) solicitudDao.loadAll();
        //data.add(new Solicitud("Juan Perez", "123456789","Calle los tres tristes indios","6984521",0.0,0.0));
        //data.add(new Solicitud("Juan Perez", "123456789","Calle los tres tristes indios","6984521",0.0,0.0));
        RequestAdapter adapter = new RequestAdapter(data, getContext());
        setListAdapter(adapter);
        getListView().setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //Toast.makeText(getActivity(), "Item: " + position, Toast.LENGTH_SHORT).show();
    }
}
